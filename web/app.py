from flask import Flask
from flask import render_template, request

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route('/', defaults={'pagename': ''})
@app.route("/<path:pagename>")
def serve_a_page(pagename):
    # print(pagename)
    name = request.environ['REQUEST_URI']
    try:
        if ("/~" in name) or ("/.." in name) or ("//" in name):
            return render_template("403.html"), 403
        else:
            return render_template("{}".format(pagename))
    except:
        #return str(name)
        return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
